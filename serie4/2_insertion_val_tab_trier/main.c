#include <stdio.h>
#define MAX_SIZE_TAB 50

int main()
{
    /**D�claration des variables*/
    int N;
    int T[MAX_SIZE_TAB+1];
    int i;
    int VAL;
    int INDEX;

    /**Saisie de N*/
    do
    {
        printf("introduire la taille de T :");
        scanf("%d",&N);
    }
    while((N<1)||(N>MAX_SIZE_TAB));

    /**Remplissage du tableau*/
    for (i=0 ; i< N; i++)
    {
        printf ("donnez le nombre num�ro %d : ", i+1) ;
        scanf ("%d",&T[i]) ;
    }
    /** Affichage du tableau*/
    printf("T[ ");
    for(i=0; i<N; i++)
    {
        printf("%d ",T[i]);
    }
    printf("]");

    /********************************************************/
    /**Ins�r�r une nouvelle valeur VAL dans le tableau tri�*/
    /*******************************************************/

    printf("\nIntroduire une valeur VAL : ");
    scanf("%d",&VAL);

    /**Recherche de l'index d'insertion dans le tableau tri� */
    for(i=0, INDEX = N; i< N ; i++)
    {
        if(T[i] > VAL)
        {
            INDEX = i;
            i = N;
        }
    }
    /** D�calage � droite*/
    for(i = N; i> INDEX; i--)
    {
        T[i] = T[i-1];
    }
    /**insertion de la valeur VAL*/
    T[INDEX] = VAL;

    /** Mettre � jour la dimension du tableau avec une case de plus ayant ajout� une valeur VAL */
    N++;

    /********************************************************/
    /** Affichage du tableau avec la nouvelle valeur*/
    /********************************************************/

    printf("Tableau Val Insert - T[ ");
    for(i=0; i<N; i++)
    {
        printf("%d ",T[i]);
    }
    printf("]");
return (0);
}//fin main
