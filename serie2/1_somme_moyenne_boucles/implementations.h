    #include <stdio.h>
    #include <stdlib.h>
    #define N 100
    #define MAX_SIZE_ARRAY 20
    #define MIN_SIZE_ARRAY 1

    #define MAX_VAL 1000
    #define MIN_VAL -1000

    #define MIN_CMD 1
    #define MAX_CMD 9

    #define FOR_SUM 1
    #define WHILE_SUM 2
    #define DOWHILE_SUM 3
    #define FOR_AVERAGE 4
    #define WHILE_AVERAGE 5
    #define DOWHILE_AVERAGE 6
    #define FOR_PRODUCT 7
    #define WHILE_PRODUCT 8
    #define DOWHILE_PRODUCT 9


    int size_array();
    int fill_array(int array[], int size_array);
    int print_array(int array[], int size_array);

    int for_sum_array(int array[], int size_array);
    int doWhile_sum_array(int array[], int size_array);
    int while_sum_array(int array[], int size_array);

    float for_average_array(int array[], int size_array);
    float doWhile_average_array(int array[], int size_array);
     float while_average_array(int array[], int size_array);

    int for_product_array(int array[], int size_array);
    int doWhile_product_array(int array[], int size_array);
    int while_product_array(int array[], int size_array);
