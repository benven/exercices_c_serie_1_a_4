

#include "implementations.h"

 /****** for product ******/
    int for_product_array(int array[], int size_array) {
        int product;

        int i;
        for (i = 0, product = array[0]; i < size_array; i++){
            if(i < size_array-1){
                product = product * array[i+1];
            }
        }
        return product;
    }

    /****** while product ******/
    int while_product_array(int array[], int size_array){
        int product;
        int i;
        i = 0;
        product = array[0];
        while (i < size_array){
            if ( i < size_array -1){
              product *= array[i+1];

            }
            i++;
        }
        return product;
    }
    /****** do while product ******/
    int doWhile_product_array(int array[], int size_array){
        int product;

        int i;
        i = 0;

        product = array[0];

        do {
            if (i < size_array-1)
            product *= array[i+1];

            i++;
        } while (i < size_array);
        return product;
    }
