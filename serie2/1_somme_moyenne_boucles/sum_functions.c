

#include "implementations.h"

/****** do while sum ******/
    int doWhile_sum_array(int array[], int size_array){
            int i;
            int sum;
            i = 0;
            sum = 0;
            printf("do while sum method :");
            do {
                sum += array[i];
                i++;
            } while(i < size_array);
            return sum;
       }
    /******  while sum ******/
    int while_sum_array(int array[], int size_array){
            int i;
            int sum;
            i = 0;
            sum = 0;
            printf("while sum method : ");
            while(i < size_array){
                sum += array[i];
                i++;
            }
            return sum;
    }
    /****** for sum ******/
    int for_sum_array(int array[], int size_array){
        int sum = 0;
        int i;
        printf("For sum method : ");
        for (i = 0, sum = 0; i < size_array; i++)
            sum += array[i];
        return sum;
    }
