
#include "implementations.h"


/****** for average ******/
    float for_average_array(int array[], int size_array){
        float average = 0.0f;
        int sum;
        int i;
        sum = for_sum_array(array, size_array);
        average = sum/size_array;
        return average;
    }
    /****** while average ******/
    float while_average_array(int array[], int size_array){
        float average = 0.0f;
        int sum;
        int i;
        sum = while_sum_array(array, size_array);
        average = sum/size_array;
        return average;
    }
    /****** do while average ******/
    float doWhile_average_array(int array[], int size_array){
        float average = 0.0f;
        int sum;
        int i;
        sum = doWhile_sum_array(array, size_array);
        average = sum/size_array;
        return average;
    }
