

#include "implementations.h"

    int main()
    {
        int array[N];
        int size;
        int cmd;

        size = size_array();
        fill_array(array, size);

        do {
            printf("1. for sum 2. while sum 3. do...while sum\n4.for average 5. while average 6. dowhile average\n 7.for product 8. while product 9. dowhile product");
            scanf("%d", &cmd);
        } while(cmd < MIN_CMD || cmd > MAX_CMD);

        switch(cmd){
        case FOR_SUM:
            printf("%d", for_sum_array(array, size), FOR_SUM);
            break;
        case WHILE_SUM:
            printf("%d", while_sum_array(array, size), WHILE_SUM);
            break;
        case DOWHILE_SUM:
            printf("%d", doWhile_sum_array(array, size), DOWHILE_SUM);
            break;
        case FOR_AVERAGE:
            printf("%.2f", for_average_array(array, size), FOR_AVERAGE);
            break;
        case WHILE_AVERAGE:
            printf("%.2f", while_average_array(array, size), WHILE_AVERAGE);
            break;
        case DOWHILE_AVERAGE:
            printf("%.2f", doWhile_average_array(array, size), DOWHILE_AVERAGE);
            break;
        case FOR_PRODUCT:
            printf("%d", for_product_array(array, size), FOR_PRODUCT);
            break;
        case WHILE_PRODUCT:
            printf("%d", while_product_array(array, size), WHILE_PRODUCT);
            break;
        case DOWHILE_PRODUCT:
            printf("%d", doWhile_product_array(array, size), DOWHILE_PRODUCT);
            break;
        default:
            printf("Incorrect value.");
            break;
        }
        return 0;
    }
