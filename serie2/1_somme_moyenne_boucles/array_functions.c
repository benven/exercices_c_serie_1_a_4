

#include "implementations.h"

/****** enter size array ******/
    int size_array(){
        int size;
        do{
            printf("Enter size : ");
            scanf("%d", &size);
        } while(size < MIN_SIZE_ARRAY || size > MAX_SIZE_ARRAY);
        return size;
    }
    /****** fill array ******/
    int fill_array(int array[], int size_array){
        int i;
        int v;

        for (i = 0; i < size_array; i++){
            do{
                printf("Enter value :");
                scanf("%d", &v);
            } while(v < MIN_VAL || v > MAX_VAL);

            array[i] = v;
        }
    }

    /****** print array ******/
    int print_array(int array[], int size_array){
        int i;
        int v;
        for (i = 0; i < size_array; i++){
          printf(" %d ", array[i]);
        }
    }
