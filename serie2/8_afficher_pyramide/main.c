#include <stdio.h>
#include <stdlib.h>

int main()
{
    int lignes;
    int space;
    int i;
    int j;
    int p;
    int stars;
    int space;
    stars = 1;
    printf("entrez lignes : ");
    scanf("%d", &lignes);
    space = lignes;
    for(i = 0; i < lignes; i++){

        for(p = space; p > 0; p--){
            printf(" ");
        }
        space--;
        for(j = 0; j < stars; j++){
            printf("%c",'*');
        }
        stars+=2;
        printf("\n");
    }
}
