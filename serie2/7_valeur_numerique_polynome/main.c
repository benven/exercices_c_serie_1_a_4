//version de mr taha

#include <stdio.h>

int main()
{
    int N;     /* degr� du polyn�me */
    float X;   /* argument          */
    float A;   /* coefficients successifs du polyn�me */
    float P;   /* coefficient courant du terme Horner */
    int i;     /* compteur */
    int N1;    /* copie degr� du polyn�me */

    do
    {
        printf("Entrer le degr�  N du polyn�me (N>0)   : ");
        scanf("%d", &N);
    }
    while(N<=0);

    N1 = N;

    printf("Entrer la valeur X de l'argument : ");
    scanf("%f", &X);

    /**1�re fa�on*/
    for(P=0.0f ; N>=0 ; N--)
    {
        printf("Entrer le coefficient A%d : ", N);
        scanf("%f", &A);
        P = P*X + A;
    }
    printf("Valeur du polyn�me pour X = %.2f : %.2f\n", X, P);

    /**2eme fa�on*/
    for(i=0, P=0.0f; i <=N1; i++)
    {
        printf("Entrer le coefficient A%d : ", i);
        scanf("%f", &A);
        P += A * pow(X,i);
    }

    printf("Valeur du polyn�me pour X = %.2f : %.2f\n", X, P);

    return (0);

}
