#include <stdio.h>
#include <stdlib.h>

#define N 100
#define MAX_SIZE_ARRAY 20
#define MIN_SIZE_ARRAY 1

#define MAX_VAL 9
#define MIN_VAL 0

#define MIN_CMD 1
#define MAX_CMD 10

/****** enter size array ******/
    int size_array(){
        int size;
        do{
            printf("Enter size : ");
            scanf("%d", &size);
        } while(size < MIN_SIZE_ARRAY || size > MAX_SIZE_ARRAY);
        return size;
    }
    /****** fill array ******/
    int fill_array(int array[], int size_array){
        int i;
        int v;

        for (i = 0; i < size_array; i++){
            do{
                printf("Enter value :");
                scanf("%d", &v);
                if(v < MIN_VAL || v > MAX_VAL ){
                    printf("Error!\n\a");
                }
            } while(v < MIN_VAL || v > MAX_VAL);

            array[i] = v;
        }
    }

    /****** print array ******/
    int print_array(int array[], int size_array){
        int i;
        int v;
        for (i = 0; i < size_array; i++){
          printf(" %d ", array[i]);
        }
    }

        int product_array(int array[], int size_array) {
        int product;

        int i;
        for (i = 0, product = array[0]; i < size_array; i++){
            if(i < size_array-1){
                product = product * array[i+1];
            }
        }
        return product;
    }

        /******sum ******/
    int sum_array(int array[], int size_array){
        int sum = 0;
        int i;

        for (i = 0, sum = 0; i < size_array; i++)
            sum += array[i];
        return sum;
    }
    /**average**/
        float average_array(int array[], int size_array){
        float average = 0.0f;
        int sum;
        int i;
        sum = sum_array(array, size_array);
        average = (float)sum/size_array;
        return average;
    }

int main()
{
    int array[N];
    int size;

    size = size_array();
    fill_array(array, size);
    print_array(array, size);
    printf("\nsum : %d\n", sum_array(array, size));
    printf("\naverage: %.2f\n",average_array(array, size));
    printf("\nproduct : %d\n", product_array(array, size));
}
