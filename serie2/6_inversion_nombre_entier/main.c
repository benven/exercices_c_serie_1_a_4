/*
Calculez le nombre lu � rebours d'un nombre positif entr� au clavier en supposant que le
fichier d'entr�e standard contient le nombre � inverser. Exemple: Entr�e: 1234 Affichage:
4321

*/

#include <stdio.h>
#include <stdlib.h>
#define NBE_MIN 0
#define NBE_MAX 100000
int main()
{
    int nbre_a_inverser;
    int resultat;
    do{
    printf("entrez un nombre � inverser : ");
    scanf("%d", &nbre_a_inverser);
    } while(nbre_a_inverser < NBE_MIN || nbre_a_inverser > NBE_MAX);
    while(nbre_a_inverser > 0) {
        printf("%d", nbre_a_inverser % 10);
        nbre_a_inverser /= 10;
    }
}
