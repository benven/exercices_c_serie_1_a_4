/*
Benjamin Venezia le 3.12.2020
Calculez la somme des N premiers termes de la s�rie harmonique, N est un param�tre donn�
par l�utilisateur � l�entr�e. 1 + 1/2 + 1/3 + ... + 1/N


*/


#include <stdio.h>
#include <stdlib.h>

float somme_harmonique_recursive(const int d, float somme, int cpt, int p){

    if(cpt < d) {
     somme = somme + (1 / (float)p);
    }
    else{
       return somme;
    }
     return somme_harmonique_recursive( d, somme, cpt+1, p+1);
}

float somme_harmonique_boucle(const int d){
    int i;
    float somme;
    for (i= 1, somme=0.0f; i <= d; i++){
        somme += (1.0/i);
    }
    return somme;
}


int main()
{
    const int d = 2; //le nombre de repetition
    float somme = 0.0f;
    int compteur = 0;
    int p = 1;

    printf("recursion : %.2f\n", somme_harmonique_recursive( d, somme, compteur, p));
    printf("boucle for : %.2f", somme_harmonique_boucle(d));
    return 0;
}
