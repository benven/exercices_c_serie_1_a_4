//Calculez par multiplications successives XN de deux entiers naturels X et N entr�s au clavier.


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/***** puissance recursion *******/
int puissance_recursion(int x, int n, int sum){
      if (n > 1){
        sum *= x;
      } else {
        return sum;
      }

    return (puissance_recursion(x, n-1, sum));
}
/******** puissance avec fonction pow ********/
int puissance_function(int x, int n){
    int result;
    result = pow(x, n);
    return result;
}


/** main **/
int main()
{
    int x;
    int n;
    long long int sum;
    int cmd;

    do {
        printf("Entrez x : ");
        scanf("%d", &x);
        sum = x;
    } while(x < -1000 || x > 1000);

    do {
        printf("Entrez n : ");
        scanf("%d", &n);
    } while(n<0 || n > 10);

    do {
        printf("Entrez cmd : ");
        scanf("%d", &cmd);
    } while(n<1 && n > 2);

    switch(cmd){
    case 1:
        printf("%d", puissance_recursion(x, n, sum));
        break;
    case 2:
        printf("%d", puissance_function(x, n));
        break;
    }
}
