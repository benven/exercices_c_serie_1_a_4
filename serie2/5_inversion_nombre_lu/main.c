/*
Calculez le nombre lu � rebours d'un nombre positif entr� au clavier en supposant que le
fichier d'entr�e standard contient une suite de chiffres non nuls, termin�e par z�ro (Contr�lez
s'il s'agit vraiment de chiffres). Exemple: Entr�e: 1 2 3 4 0 Affichage: 4321
*/
#include <stdio.h>
#include <math.h>

int main()
{
    int v;
    long affichage = 0;
    int m = 10;
    do{
        do {
            printf("Enter value : ");
            scanf("%d", &v);
        } while(v < 0 || v > 9);
            affichage += (v * m);
            m *= 10;
     }while(v!=0);

    printf("%ld", affichage / 10);
}
