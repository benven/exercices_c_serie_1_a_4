/****************************************************
Auteur : Benjamin Venezia
Cours : Taha cours Backend Crea 1�re ann�e, 1er semestre.
Objectif : - une fonction calcule le prix TTC (100 ajout� de 5% vaut 105)
           - Une autre fonction calcule le prix net (100)
*****************************************************/

#include <stdio.h>
#include <stdlib.h>

/** g�re le menu **/
#define TTC 1
#define NET 2
/** valeurs min et max  **/
#define MIN 0
#define MAX 2000
/** g�re le switch **/
#define CMDMIN 1
#define CMDMAX 2

/** prototypes **/
double calcule_prix_TTC (int pnet, int TVA);
double calcule_prix_NET (double pttc, int TVA);


int main()
{
    double prix_TTC;
    int prix_NET;
    int TVA;
    int cmd;

    do {
        printf("Entrez prix net :");
        scanf("%d", &prix_NET);
    } while(prix_NET < MIN || prix_NET > MAX);

     do {
        printf("Entrez Pourcentage TVA :");
        scanf("%d", &TVA);
    } while(TVA < MIN || TVA > MAX);

    printf("%d. prix ttc \n%d. prix net article\n", TTC, NET);
    do {
        printf("Votre choix : ");
        scanf("%d", &cmd);
    } while(cmd < CMDMIN || cmd > CMDMAX);

    switch (cmd) {
        case 1:
          prix_TTC = calcule_prix_TTC(prix_NET, TVA);
          printf("%lf", prix_TTC);
            break;
        case 2:
            prix_NET = calcule_prix_NET(calcule_prix_TTC(prix_NET, TVA), TVA);
            printf("Le prix net est de : %d", prix_NET);
                break;
        default:
            printf("Incorrect input");
        break;
    }
}
/** fonctions **/
double calcule_prix_TTC (int pnet, int TVA){
     double pttc = 0.0;
     pttc = pnet + (pnet * ((double) TVA / 100));
     return pttc;
}
double calcule_prix_NET (double pttc, int TVA){
    double pnet;
     pnet = pttc*100 / (100+TVA);
     return pnet;
}
