#include <stdio.h>
#include <stdlib.h>

int main()
{
    int quotient; //r�sultat
    int dividende;//ce qu'on divise
    int diviseur; //par quoi on divise
    int reste;    //ce qu'il reste

    do {
        printf("dividende : ");
        scanf("%d", &dividende);
    } while(dividende < 0);
    do {
        printf("diviseur : ");
        scanf("%d", &diviseur);
        if (diviseur == 0)
            printf("Division par 0!\n");
    } while(diviseur <= 0);

    reste = dividende % diviseur;
    quotient = dividende / diviseur;

    printf("%d / %d vaut : %d, le reste lui vaut : %d", dividende, diviseur, quotient, reste);
}
