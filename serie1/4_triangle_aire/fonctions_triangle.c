#include "implementations.h"

/************ perimetre du triangle *****************/
float calcule_perimetre_triangle(int a, int b, int c){
    float perimetre;
    perimetre = a + b + c;
    return perimetre;
}
/************ aire du triangle *****************/
float calcule_aire_triangle(int a, int b, int c){
    float perimetre = calcule_perimetre_triangle(a, b, c);
    float dp;
    float aire;

    dp = perimetre / 2;
    aire = (dp*(dp-a))*(dp-b)*(dp-c);
    aire = sqrt(aire);
    return (aire);
}
