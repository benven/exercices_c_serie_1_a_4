/**********************************************
Auteur : Benjamin Venezia
Cours : Taha cours Backend Crea 1�re ann�e, 1er semestre.
Objectif : calculer l'aire d'un triangle
***********************************************/

#include "implementations.h"

int main()
{
    struct triangle t1;
    float perimetre = 0.0f;

    do {
        printf("Entrez cote 1 : ");
        scanf("%d", &t1.cote1);
    } while (t1.cote1 < MIN && t1.cote1 > MAX);
      do{
        printf("Entrez cote 2 : ");
        scanf("%d", &t1.cote2);
    } while(t1.cote2 < MIN && t1.cote2 > MAX);
      do{
        printf("Entrez cote 3 : ");
        scanf("%d", &t1.cote3);
    } while(t1.cote3 < MIN && t1.cote3 > MAX);

    printf("c1 : %d c2 : %d c3 : %d", t1.cote1, t1.cote2, t1.cote3);
    printf("\nL'aire du rectangle vaut : %.2f", calcule_aire_triangle(t1.cote1, t1.cote2, t1.cote3));
}
