
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MIN 1
#define MAX 9999


struct triangle {
    int cote1;
    int cote2;
    int cote3;
};

/************ perimetre du triangle *****************/
float calcule_perimetre_triangle(int a, int b, int c);
/************ aire du triangle *****************/
float calcule_aire_triangle(int a, int b, int c);
