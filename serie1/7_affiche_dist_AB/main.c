/****************************************************
Auteur : Benjamin Venezia
Cours : Taha cours Backend Crea 1�re ann�e, 1er semestre.
Objectif : calculer une distance � l'aide points
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Coordonnes{
    int XA;
    int XB;
    int YA;
    int YB;
};

double calcule_distance(int xa, int xb, int ya, int yb){
    double distance;
    double c1;
    double c2;

    c1 = (double)xb - xa;
    c2 = (double)yb - ya;

    distance = pow(c1, 2) + pow(c2, 2);
    distance = sqrt(distance);
    return distance;
}

int main()
{
    struct Coordonnes coordonnes;


    coordonnes.XA = 10;
    coordonnes.XB = 20;
    coordonnes.YA = 30;
    coordonnes.YB = 33;

    printf("%lf", calcule_distance(coordonnes.XA, coordonnes.XB, coordonnes.YA, coordonnes.YB));



}
