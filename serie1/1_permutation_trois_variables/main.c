/**********************************************
Auteur : Benjamin Venezia
Cours : Taha cours Backend Crea 1�re ann�e, 1er semestre.
Objectif : permute 3 variables. Oblig� d'utiliser les pointeurs.
***********************************************/

#include <stdio.h>
#include <stdlib.h>

void permutation(int *a, int *b){
    int temp;
    printf("Before permutation : a : %d b : %d\n", *a, *b);

    temp = *a;
    *a = *b;
    *b = temp;
    printf("After permutation : a : %d b : %d", *a, *b);
}

int main()
{
    int a,b;

    printf("Enter a : ");
    scanf("%d", &a);

    printf("Enter b : ");
    scanf("%d", &b);

    permutation(&a, &b);

}
