/****************************************************
Auteur : Benjamin Venezia
Cours : Taha cours Backend Crea 1�re ann�e, 1er semestre.
Objectif : somme avec sauvegarde et sans sauvegarde des valeurs
*****************************************************/


#include "implementations.h"

int main()
{
    int cmd;
    int sum = 0;
    printf("1. cinq Variables\n2. deux variables\n");
    do {
        printf("Votre choix : ");
        scanf("%d", &cmd);
    } while(cmd < CMDMIN || cmd > CMDMAX);

    switch (cmd) {
    case 1:
        sum = somme_cinq_variables();
        printf("La somme en 5 variables vaut : %d", sum);
        break;
    case 2:
        sum = somme_deux_variables(4);  //4 vaut nombre de r�p�tition
        printf("La somme en 2 variables vaut : %d", sum);
        break;
    default:
        printf("Incorrect input");
    }
}
