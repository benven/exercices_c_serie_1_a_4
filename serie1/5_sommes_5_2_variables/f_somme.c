#include "implementations.h"
/***** affiche les erreurs *****/
int affiche_erreur(int n){
    if(n < 0)
    printf("Attention, le nombre %d est inferieur � %d\n", n, MIN);
    else
    printf("Attention, le nombre %d est superieur � %d\n", n, MAX);
}
/******* somme cinq variables avec sauvegarde ********/
int somme_cinq_variables(){
    int a, b, c, d;
    int sum = 0;
    do {
        printf("Entrez nombre 1 :");
        scanf("%d", &a);
        if(a < MIN || a > MAX)
            affiche_erreur(a);
    } while(a < MIN || a > MAX);
     do {
        printf("Entrez nombre 2 :");
        scanf("%d", &b);
         if(b < MIN || b > MAX)
            affiche_erreur(b);

    } while(b < MIN || b > MAX);
     do {
        printf("Entrez nombre 3 :");
        scanf("%d", &c);
         if(c < MIN || c > MAX)
            affiche_erreur(c);
    } while(c < MIN || c > MAX);
     do {
        printf("Entrez nombre 4 :");
        scanf("%d", &d);
         if(c < MIN || c > MAX)
            affiche_erreur(c);
    } while(d < MIN || d > MAX);
    sum = a + b + c + d;
    return sum;
}
/******* somme deux variables sans sauvegarde  ********/
int somme_deux_variables(int n){
    int a;
    int sum = 0;
    for (int i = 0; i < n; i++ ){
        do {
            printf("Entrez nombre %d : ", i+1);
            scanf("%d", &a);
            sum += a;
            if(a < MIN || a > MAX)
                affiche_erreur(a);
        } while(a < MIN || a > MAX);
    }
    return sum;
}
