
#include <stdio.h>
#include <stdlib.h>
#define VMIN 0
#define VMAX 10000

#define PARA 1
#define SERIE 2

double calcule_resistance_serie(double R1, double R2, double R3);
double calcule_resistance_parallele(double R1, double R2, double R3);
