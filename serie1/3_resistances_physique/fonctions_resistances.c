
double calcule_resistance_serie(double R1, double R2, double R3){
    printf("\nResistance serie : \n");
    return R1 + R2 + R3;
}

double calcule_resistance_parallele(double R1, double R2, double R3){
    printf("\nResistance parallele : \n");
    return ((R1 * R2 * R3) / (R1 * R2 + R1 * R3 + R2 * R3));
}
