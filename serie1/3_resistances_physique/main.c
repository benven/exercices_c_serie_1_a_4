//calcule les r�sistances en s�rie et parall�le
#include "implementation.h"

int main()
{
    int r1, r2, r3;
    int choice;
    int *p;

    do {
        printf("Enter r1 r2 et r3 : ");
        scanf("%d %d %d", &r1, &r2, &r3);
    } while((r1 <= VMIN || r1 > VMAX) || (r2 <= VMIN || r2 > VMAX) ||(r3 <= VMIN || r3 > VMAX));
    do {
        printf("%d. calcule resistance parallele\n %d. calcule resistance en serie\n choice : ", PARA, SERIE);
        p = &choice; //entrainement pointeur
        scanf("%d", p);
    } while(*p < 1 || *p > 2);

    switch (*p) {
        case PARA:
            printf("%lf", calcule_resistance_parallele(r1, r2, r3));
            break;
        case SERIE:
            printf("%lf", calcule_resistance_serie(r1, r2, r3));
            break;
        default:
            printf("Error in the choice.");
            break;
    }



    return 0;
}
