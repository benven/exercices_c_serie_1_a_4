/*
Ecrire un programme qui lit la dimension N d'un tableau T du type int (dimension maximale:
50 composantes), remplit le tableau par des valeurs entr�es au clavier et affiche le tableau.
Effacer ensuite toutes les occurrences de la valeur 0 dans le tableau T et tasser les �l�ments
restants. Afficher le tableau r�sultant.
*/
#include <stdio.h>
#include <stdlib.h>
#define INIT_ARRAY 100
#define MAXSARRAY 20
#define MINSARRAY 0

#define MAXV 1000
#define MINV -1000

int define_size_array(){
    int size_array;
    do {
        printf("Size array : ");
        scanf("%d", &size_array);
    } while(size_array < MINSARRAY || size_array > MAXSARRAY);
    return size_array;
}

int fill_array(int array[], int size_array){
    int i;
    int v;
    for(i = 0; i < size_array; i++) {
        do {
            printf("Enter value [%d] : ", i+1);
            scanf("%d", &v);
        } while(v < MINV || v > MAXV);
        array[i] = v;
    }
}

void print_array(int array[], int size){
    int i;
    for(i = 0; i < size ; i++)
        printf(" %d ", array[i]);
}


int tassage(int array[], int size){
    int i;
    int j;
    int cpt = 0;


    for(i = 0; i < size; i++)
    {
        if(array[i] == 0)
        {
            for(j = i; j < size-1; j++)
            {
                array[j] = array[j+1];
            }
            size--;
            if(array[i] == 0)
            {
                i--;
            }
        }
    }

    return size;


}

int main()
{
    int a[INIT_ARRAY];
    int val;
    int size;
    int size_special;
    size = define_size_array();
    fill_array(a, size);
    size_special=tassage(a, size);
    print_array(a, size_special);

    return 0;
}
