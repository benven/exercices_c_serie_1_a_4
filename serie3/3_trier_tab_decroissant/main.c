/**
 Dans un premier temps on d�finit la taille du tableau, on le remplit.
 Ensuite on tri le tableau par ordre d�croissant.
 enfin on effectue une inversion du tableau.
**/


#include <stdio.h>
#include <stdlib.h>
#define N 100
#define INIT_ARRAY 100
#define MAXSARRAY 20
#define MINSARRAY 0

#define MAXV 1000
#define MINV -1000

int define_size_array(){
    int size_array;
    do {
        printf("Size array : ");
        scanf("%d", &size_array);
    } while(size_array < MINSARRAY || size_array > MAXSARRAY);
    return size_array;
}

int fill_array(int array[], int size_array){
    int i;
    int v;
    for(i = 0; i < size_array; i++) {
        do {
            printf("Enter value [%d] : ", i+1);
            scanf("%d", &v);
        } while(v < MINV || v > MAXV);
        array[i] = v;
    }
}

void print_array(int array[], int size){
    int i;
    for(i = 0; i < size; i++)
        printf(" %d ", array[i]);
}

void sort_desc(int a[], int size){
    int i;
    int j;
    int temp;
    for(i=0; i < size; i++) {
        for(j = 0; j < size-1; j++){
          if(a[j+1] > a[j]){
            temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
          }
        }
     }
}

void inverse(int a[], int size){
    int i;
    int j;
    int temp;
    for(i = 0, j = size - 1; i < j; i++, j--) {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
    }
}


int main()
{

    int a[N];

    int max;
    int temp;
    int size;


    size = define_size_array();
    fill_array(a, size);
    printf("original array : \n");
    print_array(a, size);
    printf("\n");

    printf("array after sort: \n");
    sort_desc(a, size);
    print_array(a, size);
    printf("\n");

    printf("array inversed \n");
    inverse(a, size);
    print_array(a, size);
    printf("\n");

    return 0;
}
