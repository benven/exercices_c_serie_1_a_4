#include <stdio.h>
#include <stdlib.h>
#define INIT_ARRAY 100
#define MAXSARRAY 20
#define MINSARRAY 0

#define MAXV 1000
#define MINV -1000

int define_size_array(){
    int size_array;
    do {
        printf("Size array : ");
        scanf("%d", &size_array);
    } while(size_array < MINSARRAY || size_array > MAXSARRAY);
    return size_array;
}

int fill_array(int array[], int size_array){
    int i;
    int v;
    for(i = 0; i < size_array; i++) {
        do {
            printf("Enter value [%d] : ", i+1);
            scanf("%d", &v);
        } while(v < MINV || v > MAXV);
        array[i] = v;
    }
}

void print_array(int array[], int size){
    int i;
    for(i = 0; i < size; i++)
        printf(" %d ", array[i]);
}

int sum_array(int array[], int size){
    int i;
    int sum;
    for(i = 0, sum = 0; i < size; i++){
        sum += array[i];
    }
    return sum;
}

int main()
{
    int a[INIT_ARRAY];
    int val;
    int size;
    size = define_size_array();
    fill_array(a, size);
    print_array(a, size);
    printf("\nSum is : %d", sum_array(a, size));
    return 0;
}
