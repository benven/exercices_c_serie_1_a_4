    //Benjamin Venezia, exercice use case pr�paration � l'examen
    //Date : 30.11.2020
    //Objectif : On remplit un tableau, avec une taille d�finit. On parcours le tab
    // -- si on tombe sur un 0, on effectue une action sp�cifique --
    //- si la somme des 3 valeurs pr�c�dents le 0 sont < 900 && >0 on remplace le 0 par leurs somme
    //- si la somme des 3 valeurs pr�c�dents le 0 sont >= 900 on remplace le 0 par la case pr�c�dente
    //- si la somme des 3 valeurs pr�c�dents le 0 sont <0 on remplace le 0 par la case suivant le 0.

    #include <stdio.h>
    #include <stdlib.h>
    #define N 20 //taille maximale du tableau
    #define MIN_ARRAY 1 //taille minimale entr�e par l'utilisateur
    #define MAX_ARRAY 25 //taille maximale entr�e par l'utilisateur
    #define V_MAX 1000   //valeur maximale entr�e par l'utilisateur
    #define V_MIN -1000  //valeur minimale entr�e par l'utilisateur
    #define VERIFICATION (tableau[i-3] + tableau[i-2] + tableau[i-1]) //verification pour la fonction parcours tableau

    /********************** prototypes ********************/
    int taille_tab(const int cmd);                                         //l'utilisateur pr�cise la taille de tableau qu'il d�sire
    int remplir_tableau(int tableau[], int taille_tableau, const int cmd);   //remplit le tableau
    void afficher_tableau(int tableau[], int taille_tableau); //affiche le tableau
    void cas_0(int tableau[], int taille_tableau); //parcours le tableau (traitement
    void affiche_erreur(int cmd);

    /************ MAIN  ****************/
    int main()
    {
        //d�claration des variables
        int t[N]; //tableau avec taille maximale 20
        int t_tab;//taille d�sir�e par l'utilisateur d�finit dans la fonction remplir tableau

        //saisie des donn�es
        t_tab = taille_tab(1); //d�finit taille du tableau
        //traitement
        remplir_tableau(t, t_tab,2); // remplit le tableau
        cas_0(t, t_tab); // traiement logique
        //affichage
        afficher_tableau(t, t_tab); //affiche le tableau

        return 0;
    }
    /***************** FONCTIONS  *******************/
    /******** d�finit la taille du tableau ******/
    int taille_tab(const int cmd){
        int taille_tableau;

        do {
            printf("Entrez la taille du tableau : ");
            scanf("%d", &taille_tableau);

            if(taille_tableau < MIN_ARRAY || taille_tableau > MAX_ARRAY)
                affiche_erreur(cmd);

        } while(taille_tableau < MIN_ARRAY || taille_tableau > MAX_ARRAY);
        return taille_tableau;
    }
    /******** remplit le tableau ******/
    int remplir_tableau(int tableau[], int taille_tableau, const int cmd){
        int i;
        int valeur; //valeur que l'utilisateur rentre
        for(i = 0; i < taille_tableau; i++){
            do {
                printf("Entrez la valeur [%d] : ", i);
                scanf("%d", &valeur);
                tableau[i] = valeur;
                if(valeur < V_MIN || valeur > V_MAX)
                    affiche_erreur(2);
            } while(valeur < V_MIN || valeur > V_MAX);
        }
        return tableau;
    }
    /******** affiche le tableau ******/
    void afficher_tableau(int tableau[], int taille_tableau){
        int i;
        for (i = 0; i < taille_tableau; i++) {
            printf(" %d ",tableau[i]);
        }
    }
    /******** traitement  ******/
    void cas_0(int tableau[], int taille_tableau){
         int i;
         for(i = 0; i < taille_tableau; i++) {
            if(tableau[i] == 0 && (i >= 3 && i < taille_tableau -1)){
                if (VERIFICATION < 900 && VERIFICATION > 0) //on pr�cise avec && car sinon le else n'est jamais v�rifi� (<0)
                    tableau[i] = (tableau[i-3] + tableau[i-2] + tableau[i-1]);
                 else if (VERIFICATION >= 900)
                    tableau[i] = tableau[i-2];
                 else
                    tableau[i] = tableau[i+1];
            }
        }
    }
    /********** messages d'erreurs ***********/
    void affiche_erreur(int cmd) {
        if (cmd==1){
            printf("La taille du tableau est depassee.  (%d / %d) \n", MIN_ARRAY, MAX_ARRAY);
        } else if (cmd == 2){
            printf("La taille des valeurs ne sont pas respectees (%d/%d) \n\n",V_MIN, V_MAX);
        }

    }



