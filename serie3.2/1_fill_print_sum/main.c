    #include <stdio.h>
    #include <stdlib.h>
    #define MAX_ROWS 50
    #define MAX_COLS 50
    #define MAX_VAL 1000
    #define MIN_VAL -1000

    void fill_2d_array(int a[MAX_ROWS][MAX_COLS], int rows, int cols) {
        int val;
        int i;
        int j;
        for(i = 0; i < rows; i++) {
            for(j = 0; j < cols; j++){
                printf("Enter C[%d]L[%d] : ", i, j);
                scanf("%d", &val);
                a[i][j] = val;
            }
        }
    }
    //define cols and lines size
    void size_2d_array(int *N_rows, int *N_cols){
        printf("How many rows?[MAX:%d] : ", MAX_ROWS);
        scanf("%d", N_rows);
        printf("How many cols?[MAX:%d] : ", MAX_COLS);
        scanf("%d", N_cols);
    }
    void print_2d_array(int a[MAX_ROWS][MAX_COLS], int cols, int rows){
        int i;
        int j;
        printf("\t");
        for(i=0; i<rows;i++){
            printf("[c%d] ", i);
        }
        printf("\n");
        for(i =0; i < cols; i++) {
            printf("[r%d] ", i);
            for(j = 0; j < rows; j++){
                printf("%5d", a[i][j]);
            }
            printf("\n");
        }
    }

    int sum_2d_array(int a[MAX_ROWS][MAX_COLS], int rows, int cols){
        int i;
        int j;
        int sum;

        for(i = 0, sum = 0; i < rows; i++) {
            for(j = 0; j < cols; j++) {
                sum+=a[i][j];
            }
        }
        return printf("The sum of array is : %d ",sum);
    }

    int sum_cols_rows_2d_array(int a[MAX_ROWS][MAX_COLS], int rows, int cols){
        int i;
        int j;
        int sum_row = 0;
        int sum_col = 0;

        for(i =0; i < rows; i++) {
            for(j = 0; j < cols; j++){
               sum_row += a[i][j];
            }
             printf("\nSum of ROW %d: %d ",i, sum_row);

             sum_row = 0;
        }

        for(i =0; i < cols; i++) {
            for(j = 0; j < rows; j++){
               sum_col += a[j][i];
            }
             printf("\nSum of COLS %d: %d ",i, sum_col);

             sum_col = 0;
        }

    }

    int transfert_2darray_to_1darray(int a1d[MAX_ROWS*MAX_COLS], int a[MAX_ROWS][MAX_COLS], int rows, int cols){
        int i;
        int j;
        int k;

        for(i = 0, k = 0; i < rows; i++){
            for(j = 0; j < cols; j++, k++){
                a1d[k] = a[i][j];
            }
        }


        printf("\nONE DIMENSIONNAL ARRAY : \n");
        for(i = 0; i < cols * rows; i++){
            printf(" %d ", a1d[i]);
        }

    }

    int main()
    {
       int a[MAX_ROWS][MAX_COLS]; //initialisation du tableau
       int a1d[MAX_ROWS * MAX_COLS];
       int N_rows; //number rows
       int N_cols; //number cols

        //define size of the array
        size_2d_array(&N_rows, &N_cols);
        //fill the array
        fill_2d_array(a, N_rows, N_cols);
        //print array
        print_2d_array(a, N_rows, N_cols);
        //sum of totality
        sum_2d_array(a, N_rows, N_cols);
        //sum cols and lines
        sum_cols_rows_2d_array(a, N_rows, N_cols);
        //transfert into 1d array
        transfert_2darray_to_1darray(a1d, a, N_rows, N_cols );


        return 0;
    }
